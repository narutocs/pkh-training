import React, { Component } from 'react'
import TaskInput from "./components/TaskInput";
import "./App.css";
class App extends Component {
  render() {
    return (
      <div className="container">
        <TaskInput/>
      </div>
    );
  }
}


export default App;
