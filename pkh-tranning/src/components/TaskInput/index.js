import React, { Component } from 'react'
import { Row, Col, Typography, Button, Input, Modal, Select, } from "antd";
import callAPI from "../../utils/callAPI";
import "./styles.css";
class TaskInput extends Component {

  state = {
    visible: false,
    isPartner : false,
    name : ""
  };

  addNew = (isPartner) => {
    console.log(isPartner);
    this.setState({
      visible: true,
      isPartner
    });
  };

  handleOk = e => {
    callAPI("")
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  onChange = (e)=>{
    let target = e.target;
    let name = target.name;
    let value = target.value;
    this.setState({
      [name] : value
    })
  }

  render() {
    const { Text } = Typography;
    const { TextArea } = Input;
    const { Option } = Select;
    const {isPartner, name} = this.state;
    return (
      <div className="warper">
        <div>
          <Text>Partner: </Text>
          <Select className="cb">
            <Option>1</Option>
            <Option>1</Option>
            <Option>1</Option>
          </Select>
          <Button type="default" onClick={ ()=>this.addNew(true)}>
            ADD NEW
          </Button>
        </div>
        <div className="type">
          <Text className="text-title">Type: </Text>
          <Select className="cb">
            <Option>1</Option>
            <Option>1</Option>
            <Option>1</Option>
          </Select>
          <Button type="default" onClick={()=>this.addNew(false)} >ADD NEW</Button>
        </div>
        <div>
          <TextArea placeholder="config" row={5} className="mt-10" style={{marginTop:"10px",height:"100px"}} />
        </div>
        <div>
          <Button type="danger" className="mt-10">SAVE</Button>
          
          <Modal
            title={`ADD NEW ${isPartner ? "PARTNER" : "TYPE"}`}
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Text>name: </Text>
            <Input placeholder="name" onChange={this.onChange} name="name" value={name}/>
          </Modal>
        </div>
      </div>
    )
  }
}

export default TaskInput;