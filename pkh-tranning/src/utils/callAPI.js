import axios from "axios";
import {URL_API} from "../constants";

const callAPI = (endPoint,method,body)=>{
  axios({
    method : method,
    url :  `${URL_API}/${endPoint}`,
    data : body
  })
};

export default callAPI;