import configModel from "../models/config.model";
import mongoose from "mongoose";

export {createConfig,getAll};

const createConfig = async(req,res,next)=>{
    try{
        const {idType,idPartner,config = {}} = req.body;
        if(!idType){
            let error =  new Error();
            error.message = "idType là bắt buộc";
            error.status = 400;
            throw error;
        }
        if(!idPartner){
            let error =  new Error();
            error.message = "idPartner là bắt buộc";
            error.status = 400;
            throw error;
        }
        if(config instanceof Object){
            const newConfig = new configModel({
                _id : new mongoose.Types.ObjectId(),
                name
            });
            await newConfig.save();
            res.status(200).send({
                message : "create config success"
            })
        }else{
            let error =  new Error();
            error.message = "config là kiểu object";
            error.status = 400;
            throw error;
        }
       
    }catch(e){
        next(e);
    }
}

const getAll = async(req,res,next)=>{
    try{
        const listConfig = await configModel.find();
        res.status(200).send({
            result : listConfig
        })
    }catch(e){
        next(e);
    }
}