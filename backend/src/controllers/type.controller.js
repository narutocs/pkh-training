import typeModel from "../models/type.model";
import mongoose from "mongoose";

export {createType,getAll};

const createType = async(req,res,next)=>{
    try{
        const {name} = req.body;
        if(!name){
            let error =  new Error();
            error.message = "name là bắt buộc";
            error.status = 400;
            throw error;
        }
        const newType = new typeModel({
            _id : new mongoose.Types.ObjectId(),
            name
        });
        await newType.save();
        res.status(200).send({
            message : "create type success"
        })
    }catch(e){
        next(e);
    }
}

const getAll = async(req,res,next)=>{
    try{
        const listType = await typeModel.find();
        res.status(200).send({
            result : listType
        })
    }catch(e){
        next(e);
    }
}