import partnerModel from "../models/partner.model";
import mongoose from "mongoose";

export {createPartner,getAll};

const createPartner = async(req,res,next)=>{
    try{
        const {name} = req.body;
        if(!name){
            let error =  new Error();
            error.message = "name là bắt buộc";
            error.status = 400;
            throw error;
        }
        const newPartner = new partnerModel({
            _id : new mongoose.Types.ObjectId(),
            name
        });
        await newPartner.save();
        res.status(200).send({
            message : "create partner success"
        })
    }catch(e){
        next(e);
    }
}

const getAll = async(req,res,next)=>{
    try{
        const listPartner = await partnerModel.find();
        res.status(200).send({
            result : listPartner
        })
    }catch(e){
        next(e);
    }
}