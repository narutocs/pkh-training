import express from "express";
import morgan from "morgan";
import connect from "./sevices/databaseConncect";
import router from "./routers";
import bodyParser from "body-parser";

const app = express();
app.use(bodyParser.urlencoded({extended:false}));
app.use(morgan("dev"));

app.use("/api",router);

app.use((error,req,res,next)=>{
    let {message,status} = error;
    res.status(status).send({
        message
    })
})

connect();


app.listen(5000,()=>{
    console.log("APP START AT PORT 5000");
})