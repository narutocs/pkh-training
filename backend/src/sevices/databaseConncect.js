import mongoose from "mongoose";

const connect = ()=>{
    mongoose.connect("mongodb://localhost:27017/pkh",()=>{
        console.log("DATABASE CONNECT SUCCESS");
    });
}

export default connect;