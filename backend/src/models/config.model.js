import {Schema,model} from "mongoose";

const configSchema = new Schema({
    _id : Schema.Types.ObjectId,
    idType : {
        type : Schema.Types.ObjectId,
        ref : "TYPE"
    },
    idPartner : {
        type : Schema.Types.ObjectId,
        ref : "PARTNER"
    },
    config : Object
});

const configModel = new model("CONFIG",configSchema);

export default configModel;