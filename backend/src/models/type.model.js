import {Schema,model} from "mongoose";

const typSchema = new Schema({
    _id : Schema.Types.ObjectId,
    name : String
});

const typeModel = new model("TYPE",typSchema);

export default typeModel;