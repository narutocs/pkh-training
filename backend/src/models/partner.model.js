import {Schema,model} from "mongoose";

const partnerSchema = new Schema({
    _id : Schema.Types.ObjectId,
    name : String
});

const partnerModel = new model("PARTNER",partnerSchema);

export default partnerModel;