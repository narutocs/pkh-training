import {Router} from "express";
import {createType,getAll} from "../controllers/type.controller";

const router = Router();

router.post("/",createType);
router.get("/",getAll);

export default router;