import {Router} from "express";
import {createPartner,getAll} from "../controllers/partner.controller";

const router = Router();

router.post("/",createPartner);
router.get("/",getAll);

export default router;