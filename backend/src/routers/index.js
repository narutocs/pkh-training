import {Router} from "express";
import partnerRouter from "./partner.router";
import configRouter from "./config.router";
import typeRouter from "./type.router";

const router = Router();

router.use("/type",typeRouter);
router.use("/partner",partnerRouter);
router.use("/config",configRouter);


export default router;