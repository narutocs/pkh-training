import {Router} from "express";
import {createConfig,getAll} from "../controllers/config.controller";

const router = Router();

router.post("/",createConfig);
router.get("/",getAll);
export default router;